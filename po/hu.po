# Hungarian translation of devede
# Copyright (C) 2010 Free Software Foundation, Inc.
# This file is distributed under the same license as the devede package.
#
# Laszlo Csordas <csola48 at gmail dot com>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: DeVeDe 3.21.0 - 201203017\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-13 22:07+0200\n"
"PO-Revision-Date: 2012-03-17 17:27+0100\n"
"Last-Translator: csola48 <csola48@gmail.com>\n"
"Language-Team: csola48 <csola48@gmail.com>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Hungarian\n"
"X-Poedit-Country: HUNGARY\n"
"X-Poedit-SourceCharset: utf-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"

#: ../src/devedeng/add_files.py:44
msgid "Video files"
msgstr "Videó fájl"

#: ../src/devedeng/add_files.py:49 ../src/devedeng/ask_subtitles.py:92
#: ../src/devedeng/dvd_menu.py:146 ../src/devedeng/dvd_menu.py:158
#: ../src/devedeng/opensave.py:48
msgid "All files"
msgstr "Összes fájl"

#: ../src/devedeng/ask_subtitles.py:81
#, fuzzy
msgid "Subtitle files"
msgstr "Felirat"

#: ../src/devedeng/avconv.py:114 ../src/devedeng/ffmpeg.py:114
#, python-format
msgid "Converting %(X)s (pass 2)"
msgstr ""

#: ../src/devedeng/avconv.py:117 ../src/devedeng/ffmpeg.py:117
#, python-format
msgid "Converting %(X)s (pass 1)"
msgstr ""

#: ../src/devedeng/avconv.py:127
#, python-format
msgid "Converting %(X)s"
msgstr ""

#: ../src/devedeng/avconv.py:507
#, fuzzy, python-format
msgid "Creating menu %(X)d"
msgstr "Menü készítése %(menu_number)d"

#: ../src/devedeng/choose_disc_type.py:156
#: ../src/devedeng/choose_disc_type.py:167
#: ../src/devedeng/choose_disc_type.py:183
#: ../src/devedeng/choose_disc_type.py:194
#: ../src/devedeng/choose_disc_type.py:205
#: ../src/devedeng/choose_disc_type.py:215
#: ../src/devedeng/choose_disc_type.py:221
#: ../src/devedeng/choose_disc_type.py:227
#, python-format
msgid "\t%(program_name)s (not installed)\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:159
#: ../src/devedeng/choose_disc_type.py:170
#: ../src/devedeng/choose_disc_type.py:186
#: ../src/devedeng/choose_disc_type.py:197
#: ../src/devedeng/choose_disc_type.py:208
#: ../src/devedeng/choose_disc_type.py:218
#: ../src/devedeng/choose_disc_type.py:224
#: ../src/devedeng/choose_disc_type.py:230
#, python-format
msgid "\t%(program_name)s (installed)\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:161
#, python-format
msgid ""
"Movie identifiers (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:172
#, python-format
msgid ""
"Movie players (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:188
#, python-format
msgid ""
"Movie Converters (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:200
#, python-format
msgid ""
"CD/DVD burners (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:210
#, python-format
msgid ""
"ISO creators (install at least one of these):\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/choose_disc_type.py:233
#, python-format
msgid ""
"Other programs:\n"
"\n"
"%(program_list)s\n"
msgstr ""

#: ../src/devedeng/dvdauthor_converter.py:48
#, fuzzy
msgid "Creating DVD structure"
msgstr "DVD fa struktúra készítése"

#: ../src/devedeng/dvd_menu.py:51 ../src/devedeng/dvd_menu.py:418
msgid "Play all"
msgstr ""

#: ../src/devedeng/dvd_menu.py:93
msgid "The selected file is a video, not an audio file"
msgstr ""

#: ../src/devedeng/dvd_menu.py:93 ../src/devedeng/dvd_menu.py:96
msgid "Error"
msgstr "Hiba"

#: ../src/devedeng/dvd_menu.py:96
#, fuzzy
msgid "The selected file is not an audio file"
msgstr "Ez a fájl úgy tűnik, hogy egy hang fájl."

#: ../src/devedeng/dvd_menu.py:141
#, fuzzy
msgid "Picture files"
msgstr "Videó fájl"

#: ../src/devedeng/dvd_menu.py:153
#, fuzzy
msgid "Sound files"
msgstr "Videó fájl"

#: ../src/devedeng/dvd_menu.py:409
#, python-brace-format
msgid "Page {0} of {1}"
msgstr ""

#: ../src/devedeng/dvd_menu.py:416
msgid "Play_all"
msgstr ""

#: ../src/devedeng/file_copy.py:31
#, fuzzy, python-format
msgid "Copying file %(X)s"
msgstr "A fájl másolása"

#: ../src/devedeng/genisoimage.py:73 ../src/devedeng/mkisofs.py:73
#, fuzzy
msgid "Creating ISO image"
msgstr "ISO fájl készítése"

#: ../src/devedeng/help.py:37
#, fuzzy
msgid "Can't open the help files."
msgstr "A fájl nem nyitható."

#: ../src/devedeng/mux_dvd_menu.py:34
#, python-format
msgid "Mixing menu %(X)d"
msgstr ""

#: ../src/devedeng/opensave.py:44
msgid "DevedeNG projects"
msgstr ""

#: ../src/devedeng/project.py:231
msgid "Abort the current DVD and exit?"
msgstr "Aktuális folyamat megszakítása és kilépés?"

#: ../src/devedeng/project.py:231
msgid "Exit DeVeDe"
msgstr "Kilépés a DeVeDe programból"

#: ../src/devedeng/project.py:285 ../src/devedeng/project.py:738
msgid "In menu"
msgstr ""

#: ../src/devedeng/project.py:287
msgid "The following files could not be added:"
msgstr ""

#: ../src/devedeng/project.py:288 ../src/devedeng/project.py:746
msgid "Error while adding files"
msgstr ""

#: ../src/devedeng/project.py:299
#, python-format
msgid "The file <b>%(X)s</b> <i>(%(Y)s)</i> will be removed."
msgstr ""

#: ../src/devedeng/project.py:299
#, fuzzy
msgid "Delete file"
msgstr "Cím törlése"

#: ../src/devedeng/project.py:499
#, python-format
msgid "The limit for this format is %(l)d files, but your project has %(h)d."
msgstr ""

#: ../src/devedeng/project.py:499
msgid "Too many files in the project"
msgstr ""

#: ../src/devedeng/project.py:508
#, python-format
msgid ""
"The selected folder already exists. To create the project, Devede must "
"delete it.\n"
"If you continue, the folder\n"
"\n"
" <b>%s</b>\n"
"\n"
" and all its contents <b>will be deleted</b>. Continue?"
msgstr ""

#: ../src/devedeng/project.py:508
#, fuzzy
msgid "Delete folder"
msgstr "Cím törlése"

#: ../src/devedeng/project.py:643
msgid "Close current project and start a fresh one?"
msgstr ""

#: ../src/devedeng/project.py:643
msgid "New project"
msgstr ""

#: ../src/devedeng/project.py:678
msgid "The file already exists. Overwrite it?"
msgstr ""

#: ../src/devedeng/project.py:678
msgid "The file already exists"
msgstr ""

#: ../src/devedeng/project.py:744 ../src/devedeng/project.py:766
msgid "Page jump"
msgstr ""

#: ../src/devedeng/project.py:746
msgid "The following files in the project could not be added again:"
msgstr ""

#: ../src/devedeng/runner.py:90 ../src/devedeng/runner.py:91
#, fuzzy
msgid "Cancel the current job?"
msgstr "Feladat visszavonása?"

#: ../src/devedeng/settings.py:45 ../src/devedeng/settings.py:63
msgid "Use all cores"
msgstr ""

#: ../src/devedeng/settings.py:50
#, python-format
msgid "Use %(X)d core"
msgid_plural "Use %(X)d cores"
msgstr[0] ""
msgstr[1] ""

#: ../src/devedeng/settings.py:57
#, python-format
msgid "Use all except %(X)d core"
msgid_plural "Use all except %(X)d cores"
msgstr[0] ""
msgstr[1] ""

#: ../src/devedeng/settings.py:128
msgid "No discs supported"
msgstr ""

#: ../src/devedeng/subtitles_mux.py:43
#, fuzzy, python-format
msgid "Adding %(L)s subtitles to %(X)s"
msgstr "Felirat hozzáadása "

#: ../src/devedeng/title.py:32
#, python-format
msgid "Title %(X)d"
msgstr "Cím %(X)d"

#: ../src/devedeng/vcdimager_converter.py:48
#, fuzzy
msgid "Creating CD image"
msgstr "ISO fájl készítése"

#~ msgid "Creating BIN/CUE files"
#~ msgstr "BIN/CUE fájl készítése"

#~ msgid ""
#~ "Failed to create the BIN/CUE files\n"
#~ "Maybe you ran out of disk space"
#~ msgstr ""
#~ "Hiba a BIN/CUE fájl készítésekor\n"
#~ "Lehetséges, hogy nincs elég hely a lemezen"

#~ msgid ""
#~ "Failed to create the ISO image\n"
#~ "Maybe you ran out of disk space"
#~ msgstr ""
#~ "Hiba az ISO fájl készítésekor\n"
#~ "Lehetséges, hogy nincs elég hely a lemezen"

#~ msgid ""
#~ "Insuficient free space. To create this disc\n"
#~ "%(total)d MBytes are needed, but only %(free)d MBytes are available."
#~ msgstr ""
#~ "Nincs elég hely a lemezen.\n"
#~ "%(total)d MB lenne szükséges, de csak %(free)d MB használható."

#~ msgid ""
#~ "Failed to write to the destination directory.\n"
#~ "Check that you have privileges and free space there."
#~ msgstr ""
#~ "Hiba a cél könyvtárba történő íráskor.\n"
#~ "Ellenőrizze, hogy van-e joga az íráshoz és van-e szabad hely."

#~ msgid ""
#~ "The file or folder\n"
#~ "\n"
#~ "%(folder)s\n"
#~ "\n"
#~ "already exists. If you continue, it will be deleted."
#~ msgstr ""
#~ "A fájl vagy a gyűjtő\n"
#~ "\n"
#~ "%(folder)s\n"
#~ "\n"
#~ "már létezik. Ha folytatja, az törölve lesz."

#~ msgid "Unknown error"
#~ msgstr "Ismeretlen hiba"

#~ msgid ""
#~ "Failed to create the DVD tree\n"
#~ "Maybe you ran out of disk space"
#~ msgstr ""
#~ "Hiba a DVD fa készítésekor\n"
#~ "Nincs elég hely a lemezen."

#~ msgid ""
#~ "The menu soundtrack seems damaged. Using the default silent soundtrack."
#~ msgstr ""
#~ "A menü hangsávja sérült lehet. Használja az eredeti silent.ogg hangsávot."

#~ msgid ""
#~ "Can't find the menu background.\n"
#~ "Check the menu options."
#~ msgstr ""
#~ "Nem található a menü háttér.\n"
#~ "Ellenőrizze a menü opciókat"

#~ msgid "That file doesn't contain a disc structure."
#~ msgstr "A fájl nem tartalmaz lemez struktúrát."

#~ msgid "That file doesn't contain a DeVeDe structure."
#~ msgstr "A fájl nem tartalmaz egy DeVeDe struktúrát."

#~ msgid ""
#~ "Can't find the following movie files. Please, add them and try to load "
#~ "the disc structure again.\n"
#~ msgstr ""
#~ "Nem található a következő film fájl. Adja azt a lemez struktúrához újra.\n"

#~ msgid ""
#~ "Can't find the menu background. I'll open the disc structure anyway with "
#~ "the default menu background, so don't forget to fix it before creating "
#~ "the disc."
#~ msgstr ""
#~ "Nem található a menü háttérfájl. A lemez-szerkezetet ki lesz nyitva az "
#~ "alapértelmezett menüháttérrel, ezért ne felejtse el megjavítani azt "
#~ "mielőtt létrehozza a lemezt."

#~ msgid ""
#~ "Can't find the menu soundtrack file. I'll open the disc structure anyway "
#~ "with a silent soundtrack, so don't forget to fix it before creating the "
#~ "disc."
#~ msgstr ""
#~ "Nem található a menü hangfájl. A lemez-szerkezetet ki lesz nyitva az "
#~ "alapértelmezett silent.ogg hangsávval, ezért ne felejtse el megjavítani "
#~ "azt mielőtt létrehozza a lemezt."

#~ msgid "No filename"
#~ msgstr "Nincs fájlnév"

#~ msgid "Can't save the file."
#~ msgstr "A fájl nem menthető."

#~ msgid ""
#~ "Too many videos for this disc size.\n"
#~ "Please, select a bigger disc type or remove some videos."
#~ msgstr ""
#~ "Túl sok a videó ehhez a lemez mérethez.\n"
#~ "Válasszon egy nagyobb méretű lemez típust vagy távolítson el néhány "
#~ "videót."

#~ msgid ""
#~ "Some files weren't video files.\n"
#~ "None added."
#~ msgstr ""
#~ "Néhány fájl nem videófájl.\n"
#~ "Hozzáadás nem történt."

#~ msgid ""
#~ "Your project contains %(X)d movie files, but the maximum is %(MAX)d. "
#~ "Please, remove some files and try again."
#~ msgstr ""
#~ "A projekt %(X)d film fájlt tartalmaz, de az csak maximum %(MAX)d lehet. "
#~ "Távolítson el néhány fájlt és próbálja újra."

#~ msgid "no chapters"
#~ msgstr "nincsenek fejezetek"

#~ msgid "Unsaved disc structure"
#~ msgstr "Nem mentett lemez struktúra"

#~ msgid "Codepage"
#~ msgstr "Kódlap"

#~ msgid "Language"
#~ msgstr "Nyelv"

#~ msgid "File doesn't seem to be a video file."
#~ msgstr "Ez a fájl nem tűnik videófájlnak."

#~ msgid "Please, add only one file each time."
#~ msgstr "Egy időben csak egy fájlt adjon hozzá."

#~ msgid "Please, add a movie file before adding subtitles."
#~ msgstr "Előbb a film fájlt, utána a feliratot kell hozzáadni."

#~ msgid ""
#~ "Conversion failed.\n"
#~ "It seems a bug of SPUMUX."
#~ msgstr ""
#~ "Konverzió megszakadt.\n"
#~ "Valószínűleg hiba van a SPUMUX programban."

#~ msgid ""
#~ "File copy failed\n"
#~ "Maybe you ran out of disk space?"
#~ msgstr ""
#~ "Fájl másolása megszakadt\n"
#~ "Lehetséges, hogy nincs elég hely a lemezen?"

#~ msgid "Creating preview"
#~ msgstr "Előnézet készítése"

#~ msgid ""
#~ "Converting files from title %(title_number)s (pass %(pass_number)s)\n"
#~ "\n"
#~ "%(file_name)s"
#~ msgstr ""
#~ "Fájl konvertálása a címből %(title_number)s (átfutás %(pass_number)s)\n"
#~ "\n"
#~ "%(file_name)s"

#~ msgid ""
#~ "Conversion failed.\n"
#~ "It seems a bug of Mencoder."
#~ msgstr ""
#~ "Konverzió megszakadt.\n"
#~ "Valószínűleg hiba van az Mencoder programban."

#~ msgid "Also check the extra params passed to Mencoder for syntax errors."
#~ msgstr ""
#~ "Szükséges az Mencoder extra paramétereinek szintaxis szempontjából "
#~ "történő ellenőrzése."

#~ msgid ""
#~ "Conversion failed\n"
#~ "Maybe you ran out of disk space?"
#~ msgstr ""
#~ "Konverzió megszakadt\n"
#~ "Lehet, hogy nincs elég hely a lemezen?"

#~ msgid "Failed to create the menues."
#~ msgstr "Hiba a menü készítésénél."

#~ msgid "Menu generation failed."
#~ msgstr "Menü készítése hibás."

#~ msgid ""
#~ "Can't add the buttons to the menus.\n"
#~ "It seems a bug of SPUMUX."
#~ msgstr ""
#~ "Nem tudja hozzáadni a gombokat a menühöz.\n"
#~ "Ez a SPUMUX program hibájának tűnik."

#~ msgid ""
#~ "Your FFMPEG version is compiled\n"
#~ "without MP3LAME support, so\n"
#~ "Devede can't create DivX.\n"
#~ "\n"
#~ "Recompile your FFMPEG code, or\n"
#~ "install a version with MP3LAME\n"
#~ "support."
#~ msgstr ""
#~ "Az ön FFMpeg verziója MP3LAME \n"
#~ "támogatás nélkül van készítve, ezért a\n"
#~ "Devede nem tud DivX-et készíteni\n"
#~ "Készítsen saját FFMPEG kódot vagy \n"
#~ "installálja az MP3LAME támogatéást"

#~ msgid ""
#~ "This version of Devede allows to use FFMPEG instead\n"
#~ "of Mencoder for movie conversion. This code is still\n"
#~ "EXPERIMENTAL, but quite complete.\n"
#~ "\n"
#~ "If you see glitches in the video when using FFMPEG, try\n"
#~ "to disable the multicore support before filling a bug.\n"
#~ "\n"
#~ "To test this mode, active it in Edit->Preferences"
#~ msgstr ""
#~ "A DeVeDe ezen verziója megengedi az FFMpeg használatát\n"
#~ "az Mencoder helyett a film konverziójához. Ez a kód még\n"
#~ "KÍSÉRLETI, de megfelelően használható\n"
#~ "Ha hibát észlel a videóban az FFMpeg használatával,\n"
#~ "akkor kapcsolja ki a többmagos támogatást\n"
#~ "A kísérleti mód aktiválása: Szerkesztés -> Beállítások "

#~ msgid "Don't show this message at startup again"
#~ msgstr "Ne mutassa ezt az üzenetet többet"

#~ msgid "Erase temporary files"
#~ msgstr "Ideiglenes fájlok törlése"

#~ msgid "Use optimizations for multicore CPUs"
#~ msgstr "Többmagú CPU optimalizálás választása"

#, fuzzy
#~ msgid "Use FFMPEG instead of Mencoder for videos"
#~ msgstr "FFMPEG használata az Mencoder helyett"

#, fuzzy
#~ msgid "Use FFMPEG instead of Mencoder for menu"
#~ msgstr "FFMPEG használata az Mencoder helyett"

#~ msgid "<b>Options</b>"
#~ msgstr "<b>Opciók</b>"

#~ msgid "Temporary files folder:"
#~ msgstr "Ideiglenes fájlok gyűjtője:"

#~ msgid "Folder for temporary file"
#~ msgstr "Az ideiglenes fájlok gyűjtője"

#~ msgid "<b>Folders</b>"
#~ msgstr "<b>Gyűjtők</b>"

#~ msgid ""
#~ "AC3_FIXED workaround to fix discs without sound when using Mencoder beta"
#~ msgstr ""
#~ "Az AC3 fix áthidaló megoldás hang nélkül a Mencoder beta használatához"

#~ msgid "<b>Workarounds</b>"
#~ msgstr "<b>Áthidaló megoldások</b>"

#~ msgid "File:"
#~ msgstr "Fájl:"

#~ msgid "Clear"
#~ msgstr "Törlés"

#~ msgid "Encoding:"
#~ msgstr "Kódolás:"

#~ msgid "Language:"
#~ msgstr "Nyelv:"

#~ msgid "Put subtitles upper"
#~ msgstr "Tegye a feliratot felülre"

#~ msgid "Add subtitles"
#~ msgstr "Felirat hozzáadása"

#~ msgid "Choose a file"
#~ msgstr "Egy fájl választása"

#~ msgid "Aborted. There can be temporary files at the destination directory."
#~ msgstr "Megszakítás. Az ideiglenes fájlok a célkönyvtárban."

#~ msgid "Delete chapter"
#~ msgstr "Fejezet törlése"

#~ msgid ""
#~ "You chose to delete this chapter and\n"
#~ "reorder the other ones:"
#~ msgstr ""
#~ "Kiválasztott fejezet törlése és\n"
#~ "egy új fejezet megadása:"

#~ msgid "Proceed?"
#~ msgstr "Folytatás?"

#~ msgid "Delete subtitle"
#~ msgstr "Felirat törlése"

#~ msgid "Remove the selected subtitle file?"
#~ msgstr "A kijelölt felirat fájl eltávolítása?"

#~ msgid ""
#~ "You chose to delete this title and\n"
#~ "reorder the other ones:"
#~ msgstr ""
#~ "Kiválasztott cím törlése és\n"
#~ "egy új cím megadása:"

#~ msgid "Disc type selection"
#~ msgstr "Lemez típus kiválasztása"

#~ msgid "<b>Choose the disc type you want to create with DeVeDe</b>"
#~ msgstr "<b>Lemez típus választása, amelyre a DVD-t készíti a DeVeDe-vel</b>"

#~ msgid ""
#~ "<b>Video DVD</b>\n"
#~ "Creates a video DVD suitable for all DVD home players"
#~ msgstr ""
#~ "<b>Videó DVD</b>\n"
#~ "Videó DVD készítése a legtöbb otthoni DVD lejátszóhoz"

#~ msgid ""
#~ "<b>VideoCD</b>\n"
#~ "Creates a VideoCD, with a picture quality equivalent to VHS"
#~ msgstr ""
#~ "<b>Videó CD</b>\n"
#~ "Videó CD készítése VHS képminőséggel"

#~ msgid ""
#~ "<b>Super VideoCD</b>\n"
#~ "Creates a VideoCD with better picture quality"
#~ msgstr ""
#~ "<b>Super Videó CD</b>\n"
#~ "Videó CD készítése a legjobb képminőséggel"

#~ msgid ""
#~ "<b>China VideoDisc</b>\n"
#~ "Another kind of Super VideoCD"
#~ msgstr ""
#~ "<b>China Videó Disc</b>\n"
#~ "vagy másféle Super Videó CD készítése"

#~ msgid ""
#~ "<b>DivX / MPEG-4</b>\n"
#~ "Creates files compliant with DivX home players"
#~ msgstr ""
#~ "<b>DivX / MPEG-4</b>\n"
#~ "DVD készítése DivX-képes otthoni lejátszókhoz"

#~ msgid ""
#~ "Your DVD structure contains empty titles.\n"
#~ "Continue anyway?"
#~ msgstr ""
#~ "A DVD struktúra üres címeket tartalmaz.\n"
#~ "Folytatja?"

#~ msgctxt "Means:"
#~ msgid "Burn"
#~ msgstr "A lemez írása"

#~ msgid "Job done!"
#~ msgstr "Feladat rendben!"

#~ msgid "Delete current disc structure?"
#~ msgstr "A jelenlegi lemez struktúra törlése?"

#~ msgid "Delete the current disc structure and start again?"
#~ msgstr "A jelenlegi lemez struktúra törlése és újraindítás?"

#~ msgid "File properties"
#~ msgstr "Fájl jellemzők"

#~ msgid "<b>File</b>"
#~ msgstr "<b>Fájl</b>"

#~ msgid "Frames per second:"
#~ msgstr "Képkocka másodpercenként:"

#~ msgid "Original length (seconds):"
#~ msgstr "Eredeti hossz (másodperc):"

#~ msgid "Original audio rate (Kbits/sec):"
#~ msgstr "Eredeti hang sebesség (kbit/sec):"

#~ msgid "Original video rate (Kbits/sec):"
#~ msgstr "Eredeti videó sebesség (kbit/sec):"

#~ msgid "Original size (pixels):"
#~ msgstr "Eredeti méret (pixel):"

#~ msgid "Final size (pixels):"
#~ msgstr "Kész méret (pixelben):"

#~ msgid "Estimated final length (MBytes):"
#~ msgstr "Becsült végső hossz (MB):"

#~ msgid "<b>File info</b>"
#~ msgstr "<b>Fájl infó</b>"

#~ msgid "PAL/SECAM"
#~ msgstr "PAL/SECAM"

#~ msgid "NTSC"
#~ msgstr "NTSC"

#~ msgid "<b>Video format</b>"
#~ msgstr "<b>Videó formátum</b>"

#~ msgid "Audio track:"
#~ msgstr "Hangsáv:"

#~ msgid "This video file has no audio tracks."
#~ msgstr "Ennek a videó fájlnak nincs hangsávja."

#~ msgid "<b>Audio tracks</b>"
#~ msgstr "<b>Hang sáv</b>"

#~ msgid "Reset"
#~ msgstr "Törlés"

#~ msgid "Resets the volume to 100%"
#~ msgstr "Hangerő visszaállítása 100%-ra"

#~ msgid "<b>Volume (%)</b>"
#~ msgstr "<b>Hangerő (%)</b>"

#~ msgid "Store full length"
#~ msgstr "A teljes hossz tárolása"

#~ msgid "Store the first half"
#~ msgstr "A teljes hossz első felének tárolása"

#~ msgid "Store the second half"
#~ msgstr "A teljes hossz második felének tárolása"

#~ msgid "<b>File split</b>"
#~ msgstr "<b>Fájl szeletelés</b>"

#~ msgid "Font size:"
#~ msgstr "Betűméret:"

#~ msgid "Force subtitles"
#~ msgstr "Felirat kényszerítése"

#~ msgid "DivX format doesn't support subtitles. Please, read the FAQ."
#~ msgstr "DivX formátum nem támogatja a feliratot. Olvassa el a FAQ-ot."

#~ msgid "<b>Subtitles</b>"
#~ msgstr "<b>Felirat</b>"

#~ msgid "<b>Video rate (Kbits/sec)</b>"
#~ msgstr "<b>Videó sebesség (kbit/sec)</b>"

#~ msgid "<b>Audio rate (Kbits/sec)</b>"
#~ msgstr "<b>Hangsebesség (kbit/sec)</b>"

#~ msgid "Split the file in chapters (for easy seeking)"
#~ msgstr "A fejezetekben a fájl felosztása (könnyebb keresés)"

#~ msgid "Size (in minutes) for each chapter:"
#~ msgstr "Minden fejezet mérete (percben):"

#~ msgid "<b>Division in chapters</b>"
#~ msgstr "<b>Fejezetek felosztása</b>"

#~ msgid "General"
#~ msgstr "Általános"

#~ msgid "352x240"
#~ msgstr "352x240"

#~ msgid "Default"
#~ msgstr "Alapértelmezés"

#~ msgid "352x480"
#~ msgstr "352x480"

#~ msgid "480x480"
#~ msgstr "480x480"

#~ msgid "720x480"
#~ msgstr "720x480"

#~ msgid "704x480"
#~ msgstr "704x480"

#~ msgid "1920x1080"
#~ msgstr "1920x1080"

#~ msgid "1280x720"
#~ msgstr "1280x720"

#~ msgid "160x128"
#~ msgstr "160x128"

#~ msgid "<b>Final size</b>"
#~ msgstr "<b>Végső méret</b>"

#~ msgid "4:3"
#~ msgstr "4:3"

#~ msgid "16:9"
#~ msgstr "16:9"

#~ msgid "<b>Aspect ratio</b>"
#~ msgstr "<b>Kép arány</b>"

#~ msgid "Video format"
#~ msgstr "Videó formátum"

#~ msgid "No rotation"
#~ msgstr "Nincs forgatás"

#~ msgid "Rotate 180 degrees"
#~ msgstr "Elforgatás 180 fokkal"

#~ msgid "Rotate 90 degrees clockwise"
#~ msgstr "Elforgatás 90 fokkal órajárással egyező irányban"

#~ msgid "Rotate 90 degrees counter-clockwise"
#~ msgstr "Elforgatás 90 fokkal órajárással ellentétes irányban"

#~ msgid "<b>Rotation</b>"
#~ msgstr "<b>Forgatás</b>"

#~ msgid "Horizontal mirror"
#~ msgstr "Vízszintes tükrözés"

#~ msgid "Vertical mirror"
#~ msgstr "Függőleges tükrözés"

#~ msgid "<b>Mirror</b>"
#~ msgstr "<b>Tükrözés</b>"

#~ msgid "Swap fields"
#~ msgstr "Mezők cseréje"

#~ msgid "<b>Field order</b>"
#~ msgstr "<b>Mező szabályok</b>"

#~ msgid "Add black bars"
#~ msgstr "Fekete sáv hozzáadása"

#~ msgid "Scale picture"
#~ msgstr "Kép mértezése"

#~ msgid "<b>Scaling mode</b>"
#~ msgstr "<b>Méretezési módszer</b>"

#~ msgid "Video options"
#~ msgstr "Videó opciók"

#~ msgid "Use MBCMP (faster)"
#~ msgstr "MBCMP használata (gyorsabb)"

#~ msgid "Select MBD mode which needs the fewest bits"
#~ msgstr "MBD módszer választása, amely a legkevesebb bitet igényli"

#~ msgid ""
#~ "Select the MBD mode which has the best rate distortion (better quality)"
#~ msgstr ""
#~ "MBD módszer választása, amely a legkisebb torzítást adja (jobb minőség)"

#~ msgid ""
#~ "Use Trellis Searched Quantization (better quality, slower conversion)"
#~ msgstr ""
#~ "Trellis kereső quantálás használata (jobb minőség, lassabb konverzió)"

#~ msgid ""
#~ "<b>MacroBlock decision algorithm and Trellis searched quantization</b>"
#~ msgstr "<b>MacroBlock döntési algoritmus és Trellis quantálás</b>"

#~ msgid "Use dual pass encoding (better quality, much slower conversion)"
#~ msgstr ""
#~ "Kettős átfutású kódolás használata (jobb minőség, de lassabb konverzió)"

#~ msgid "Turbo 1st pass"
#~ msgstr "Turbó első átfutás"

#~ msgid "<b>Two pass encoding</b>"
#~ msgstr "<b>Két átfutásos kódolás</b>"

#~ msgid "Median deinterlacing filter"
#~ msgstr "Median deinterlace szűrő"

#~ msgid "Don't deinterlace"
#~ msgstr "Ne legyen deinterlace"

#~ msgid "YADIF deinterlacing filter"
#~ msgstr "YADIF deinterlace szűrő"

#~ msgid "FFMpeg deinterlacing filter"
#~ msgstr "FFMpeg deinterlace szűrő"

#~ msgid "Linear blend"
#~ msgstr "Lineáris keverés"

#~ msgid "Lowpass5 deinterlacing filter"
#~ msgstr "Lowpass5 deinterlace szűrő"

#~ msgid "<b>Deinterlacing</b>"
#~ msgstr "<b>Deinterlace</b>"

#~ msgid "Quality"
#~ msgstr "Minőség"

#~ msgid "Audio delay (in seconds):"
#~ msgstr "Hang késleltetése (másodpercben):"

#~ msgid "Create DVD with 5.1 channel sound"
#~ msgstr "DVD készítése 5.1 csatornás hanggal"

#~ msgid ""
#~ "This file already has AC3 sound (copy audio data instead of recompress it)"
#~ msgstr "Ez a fájl már AC3 hanggal rendelkezik"

#~ msgid "<b>Audio</b>"
#~ msgstr "<b>Hang</b>"

#~ msgid "Audio"
#~ msgstr "Hang"

#~ msgid "This file is already a DVD/xCD-suitable MPEG-PS file"
#~ msgstr "Ez a fájl már DVD/xCD-képes MPEG-PS fájl"

#~ msgid "Repack audio and video without reencoding (useful for VOB files)"
#~ msgstr "Videó és hang újracsomagolása (felhasználva a VOB fájlokat)"

#~ msgid "Use a GOP of 12 frames (improves compatibility)"
#~ msgstr "12 keretes GOP használata (javítja a kompatibilitást)"

#~ msgid "<b>Special</b>"
#~ msgstr "<b>Speciális</b>"

#~ msgid "Main (-X -Y)"
#~ msgstr "Menü (-X -Y)"

#~ msgid "-VF (X,Y)"
#~ msgstr "-VF (X,Y)"

#~ msgid "-LAVCOPTS (X:Y)"
#~ msgstr "-LAVCOPTS (X:Y)"

#~ msgid "-LAMEOPTS (X:Y)"
#~ msgstr "-LAMEOPTS (X:Y)"

#~ msgid "<b>Extra parameters for coder</b>"
#~ msgstr "<b>Extra paraméterek a kódoláshoz</b>"

#~ msgid "Misc"
#~ msgstr "Különfélék"

#~ msgid "Advanced options"
#~ msgstr "További opciók"

#~ msgid "Preview"
#~ msgstr "Előnézet"

#~ msgid ""
#~ "Choose the folder where DeVeDe will create the DVD image and a name for "
#~ "it. Don't use a folder in a VFAT/FAT32 drive. Characters /, | and \\ are "
#~ "not allowed and will be replaced by underscores."
#~ msgstr ""
#~ "Válasszon egy gyűjtőt és adjon meg egy nevet, ahová a DeVeDe elkészíti a "
#~ "DVD képet. Nem lehet ez VFAT/FAT32 fájlrendszeren. A /, | és \\ "
#~ "karakterek nem megengedettek és _-re lesznek cserélve"

#~ msgid "Choose a folder"
#~ msgstr "Válasszon egy gyűjtőt"

#~ msgid "Shutdown computer when disc is done"
#~ msgstr "A számítógép kikapcsolásaa, mikor a lemezírás kész"

#~ msgid "Load a disc structure"
#~ msgstr "A lemez struktúra betöltése"

#~ msgid "Warning: you already have a disc structure"
#~ msgstr "Vigyázat: a lemezstruktúra már létezik"

#~ msgid ""
#~ "If you load a disc structure you will loose your current structure "
#~ "(unless you saved it). Continue?"
#~ msgstr ""
#~ "Amennyiben betölti a lemezstruktúrát, úgy a jelenlegi még nem mentett "
#~ "struktúra el fog veszni. Folytatja?"

#~ msgid "185 MB CD"
#~ msgstr "185 MB CD"

#~ msgid "650 MB CD"
#~ msgstr "650 MB CD"

#~ msgid "700 MB CD"
#~ msgstr "700 MB CD"

#~ msgid "1.4 GB DVD"
#~ msgstr "1.4 GB DVD"

#~ msgid "4.7 GB DVD"
#~ msgstr "4.7 GB DVD"

#~ msgid "8.5 GB DVD"
#~ msgstr "8.5 GB DVD"

#~ msgid "DeVeDe"
#~ msgstr "DeVeDe"

#~ msgid "_File"
#~ msgstr "_Fájl"

#~ msgid "_Edit"
#~ msgstr "Sz_erkesztés"

#~ msgid "_Preferences"
#~ msgstr "_Beállítások"

#~ msgid "_Help"
#~ msgstr "_Súgó"

#~ msgid "<b>Titles</b>"
#~ msgstr "<b>Cím</b>"

#~ msgid "<b>Files</b>"
#~ msgstr "<b>Fájlok</b>"

#~ msgid "Length (seconds):"
#~ msgstr "Hossz (másodperc):"

#~ msgid "Video rate (Kbits/sec):"
#~ msgstr "Videó sebesség (kbit/sec):"

#~ msgid "Audio rate (Kbits/sec):"
#~ msgstr "Hang sebesség (kbit/sec):"

#~ msgid "Estimated length (MBytes):"
#~ msgstr "Becsült hossz (MB):"

#~ msgid "Output aspect ratio:"
#~ msgstr "Kimenő képarány:"

#~ msgid "Size of chapters:"
#~ msgstr "A fejezet hossza:"

#~ msgid "Adjust disc usage"
#~ msgstr "Lemez használat beállítása"

#~ msgid "0.0%"
#~ msgstr "0.0%"

#~ msgid "Media size:"
#~ msgstr "Média mérete:"

#~ msgid "<b>Disc usage</b>"
#~ msgstr "<b>Lemez használat</b>"

#~ msgid "PAL"
#~ msgstr "PAL"

#~ msgid "<b>Default format</b>"
#~ msgstr "<b>Alapértelmezett formátum</b>"

#~ msgid "Create a menu with the titles"
#~ msgstr "Menü készítése a címekkel"

#~ msgid "Menu options"
#~ msgstr "Menü opciók"

#~ msgid "Preview menu"
#~ msgstr "Menü előnézete"

#~ msgid "<b>Menus</b>"
#~ msgstr "<b>Menü</b>"

#~ msgid "Only convert film files to compliant MPEG files"
#~ msgstr "Csak a film fájl konvertálása MPEG fájlba"

#~ msgid "Create disc structure"
#~ msgstr "Lemez struktúra felépítése"

#~ msgid "Create an ISO or BIN/CUE image, ready to burn to a disc"
#~ msgstr "ISO vagy BIN/CUE kép készítése a közvetlen lemezre való íráshoz"

#~ msgid "<b>Action</b>"
#~ msgstr "<b>Akció</b>"

#~ msgid "Menu preview"
#~ msgstr "A menü előnézete"

#~ msgid "Shadow color:"
#~ msgstr "Árnyék színe:"

#~ msgid "Text color:"
#~ msgstr "A szöveg színe:"

#~ msgid "Text:"
#~ msgstr "Szöveg:"

#~ msgid "Font:"
#~ msgstr "Betűtípus:"

#~ msgid "<b>Menu title</b>"
#~ msgstr "<b>Menü címe</b>"

#~ msgid "Select a picture to be used as background for disc's menu"
#~ msgstr "Válasszon egy képet a lemez menü hátterének"

#~ msgid "Set default background"
#~ msgstr "Háttér alapbeállítás"

#~ msgid "<b>Menu background (only PNG)</b>"
#~ msgstr "<b>Menü háttér (csak PNG kép)</b>"

#~ msgid "Choose a music file"
#~ msgstr "Válasszon egy zene fájlt"

#~ msgid "Set menus without sound"
#~ msgstr "Menü beállítása hang nélkül"

#~ msgid "<b>Menu music</b>"
#~ msgstr "<b>Menü zene</b>"

#~ msgid "<b>Horizontal</b>"
#~ msgstr "<b>Vízszintes</b>"

#~ msgid "<b>Vertical</b>"
#~ msgstr "<b>Függőleges</b>"

#~ msgid "Top"
#~ msgstr "Teteje"

#~ msgid "Middle"
#~ msgstr "Közép"

#~ msgid "Bottom"
#~ msgstr "Alul"

#~ msgid "Left"
#~ msgstr "Bal"

#~ msgid "Center"
#~ msgstr "Középen"

#~ msgid "Right"
#~ msgstr "Jobb"

#~ msgid "<b>Margins (%)</b>"
#~ msgstr "<b>Margó (%)</b>"

#~ msgid "<b>Menu position</b>"
#~ msgstr "<b>Menü helyzete</b>"

#~ msgid "Color for unselected titles:"
#~ msgstr "A nem választott címek színe:"

#~ msgid "Color for shadows:"
#~ msgstr "Az árnyék színe:"

#~ msgid "Color for selected title:"
#~ msgstr "A választott cím színe:"

#~ msgid "Background color for titles:"
#~ msgstr "A cím hátterének színe:"

#~ msgid "<b>Menu font and colors</b>"
#~ msgstr "<b>Menü betűtípus és színek</b>"

#~ msgid "Show menu at disc startup"
#~ msgstr "Menü mutatása indításkor"

#~ msgid "Jump to the first title at startup"
#~ msgstr "Induláskor az első címre ugrás"

#~ msgid "<b>Disc startup options</b>"
#~ msgstr "<b>Lemez indulási opciók</b>"

#~ msgid "Can't find the font for subtitles. Aborting."
#~ msgstr "Nem található a felirat betűtípusa. Feladat törölve."

#~ msgid "Replay the preview again?"
#~ msgstr "Az előnézet megismétlése?"

#~ msgid "<b>Preview video</b>"
#~ msgstr "<b>Videó előnézet</b>"

#~ msgid ""
#~ "DeVeDe will create a preview with the selected parameters, so you will be "
#~ "able to check video quality, audio sync and so on."
#~ msgstr ""
#~ "DeVeDe készít egy előnézetet a megadott paraméterekkel, hogy ellenőrizni "
#~ "lehessen a videó és a hang minőséget, valamint mást is."

#~ msgid "Preview length:"
#~ msgstr "Előnézet hossza:"

#~ msgid "Can't find the following programs:"
#~ msgstr "Nem érhető el a következő program:"

#~ msgid ""
#~ "DeVeDe needs them in order to work. If you want to use DeVeDe, you must "
#~ "install all of them."
#~ msgstr ""
#~ "A DeVeDe megfelelő működéséhez minden komponens telepítése szükséges."

#~ msgid "Creating..."
#~ msgstr "Készítés..."

#~ msgid "Save current disc structure"
#~ msgstr "Az aktuális lemez struktúra mentése"

#~ msgid "Title properties"
#~ msgstr "Cím tulajdonságai"

#~ msgid "<b>Title's name</b>"
#~ msgstr "<b>Címek</b>"

#~ msgid "Stop reproduction/show disc menu"
#~ msgstr "Reprodukció megállítása/lemez menü mutatása"

#~ msgid "Play the first title"
#~ msgstr "Az első cím játszása"

#~ msgid "Play the previous title"
#~ msgstr "Az előző cím játszása"

#~ msgid "Play this title again (loop)"
#~ msgstr "Ezen cím újra játszása (ismétlés)"

#~ msgid "Play the next title"
#~ msgstr "A következő cím játszása"

#~ msgid "Play the last title"
#~ msgstr "Az utolsó cím játszása"

#~ msgid "<b>Action to perform when this title ends</b>"
#~ msgstr "<b>Teendő, amikor ezen cím lejátszása véget ér</b>"

#~ msgid "Warning"
#~ msgstr "Figyelmeztetés"

#~ msgid "<b>Vertical alignment</b>"
#~ msgstr "<b>Függőleges igazítás</b>"
